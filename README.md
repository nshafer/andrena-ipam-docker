# Quick development phpIPAM instance

> DO NOT RUN THIS IN PRODUCTION. It is only for local development and testing.

Run this with:

    docker-compose up -d

Connect to the interface: http://localhost:8080/

Upon initial creation, choose the following options:

1. New phpipam installation
2. Automatic database installation
    - Username: root
    - Password: andrena_mysql_root
3. Postinstall configuration
    - Whatever password you want
    - Site title: Andrena Dev phpIPAM
    - Site URL: http://localhost:8080/
4. Login. Username is "admin" by default.
5. Administration -> phpIPAM settings
    - Inactivity timeout: 12 hours
    - API: ON
    - Customers module: OFF
    - Require unique subnets: OFF
6. Administration -> API
    - App id: andrenadev
    - App permissions: Read / Write / Admin
    - App security: SSL with App code token

For your local andrena.tech configuration, add this to your .env:

    # PHP IPAM integration
    IPAM_HOST=https://localhost:8443
    IPAM_APP_ID=andrenadev
    IPAM_TOKEN=your_generated_token
    IPAM_SSL_VERIFY=False

Ports:

- 8000: non-SSL interface
- 8443: SSL interface for the API requests

> Note:
> 
> PHP IPAM does not really work through the SSL port, as it is setting a base href in the HTML that does not include the port.
> So you need to access the interface through http://localhost:8080 
